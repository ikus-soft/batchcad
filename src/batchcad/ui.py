# -*- coding: utf-8 -*-
# BatchCAD - AutCAD Automation
# Copyright (C) 2022 IKUS Soft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import tkinter
import tkinter.filedialog
import tkinter.messagebox
import webbrowser
from gettext import gettext as _

import pkg_resources
import tkvue
from pyautocad import APoint, Autocad

from batchcad.core import PLOT_TYPE_EXTENDS, PLOT_TYPE_WINDOW, StyleSheetError, plot
from batchcad.latest import LatestCheck, LatestCheckFailed

logger = logging.getLogger(__name__)

tkvue.configure_tk(basename="BatchCAD", classname="BatchCAD", theme='clam')  # TODO


class HomeDialog(tkvue.Component):
    template = """
    <TopLevel title="BatchCAD">
        <Frame pack-padx="5" pack-pady="5" pack-fill="x">
            <Frame pack-fill="x" pack-pady="2">
                <Label text="Printer / plotter: " pack-side="left" width="20" />
                <ComboBox pack-side="left" pack-fill="x" pack-expand="1" value="{{plot_devices}}" textvariable="{{plot_device}}" state="{{state}}" />
            </Frame>
            <Frame pack-fill="x" pack-pady="2">
                <Label text="Paper size: " pack-side="left" width="20" />
                <ComboBox pack-side="left"pack-fill="x" pack-expand="1" value="{{paper_sizes}}" textvariable="{{paper_size}}" state="{{state}}"/>
            </Frame>
            <Frame pack-fill="x" pack-pady="2">
                <Label text="Plot Style: " pack-side="left" width="20" />
                <ComboBox pack-side="left" pack-fill="x" pack-expand="1" value="{{style_sheets}}" textvariable="{{style_sheet}}" state="{{state}}"/>
            </Frame>
            <LabelFrame pack-fill="x" pack-pady="2" text="Drawing orientation">
                <RadioButton pack-fill="x" pack-expand="1" text="Portrait" value="0" variable="{{plot_rotation}}"/>
                <RadioButton pack-fill="x" pack-expand="1" text="Landscape" value="1" variable="{{plot_rotation}}"/>
            </LabelFrame>
            <LabelFrame pack-fill="x" pack-pady="2" text="Plot Area">
                <Checkbutton text="Printer using window coordinates:" pack-fill="x" variable="{{plot_window}}"/>
                <Frame pack-fill="x" pack-pady="2">
                    <Label text="Lower left: " pack-side="left" width="20" />
                    <Entry pack-side="left" pack-fill="x" pack-expand="1" pack-padx="2" textvariable="{{x1}}" state="{{plot_window_state}}"/>
                    <Entry pack-side="left" pack-fill="x" pack-expand="1" textvariable="{{y1}}" state="{{plot_window_state}}"/>
                </Frame>
                <Frame pack-fill="x" pack-pady="2">
                    <Label text="Top right: " pack-side="left" width="20" />
                    <Entry pack-side="left" pack-fill="x" pack-expand="1" pack-padx="2" textvariable="{{x2}}" state="{{plot_window_state}}"/>
                    <Entry pack-side="left" pack-fill="x" pack-expand="1" textvariable="{{y2}}" state="{{plot_window_state}}"/>
                </Frame>
            </LabelFrame>
            <Frame pack-fill="x" pack-pady="2" >
                <Button text="Start" pack-side="right"  command="_on_start"/>
                <Button text="Quit" pack-side="right" pack-padx="2" command="_on_quit" />
                <Label text="Processing..." pack-side="right" pack-padx="2" visible="{{is_running}}" />
            </Frame>
        </Frame>
    </TopLevel>
    """

    def __init__(self, *args, **kwargs):
        self.data = tkvue.Context(
            {
                # List of files to be printed
                'filenames': [],
                # available autocad settings
                'plot_devices': [],
                'paper_sizes_table': [],
                'paper_sizes': [],
                'style_sheets': [],
                # setting selected
                'plot_device': '',
                'paper_size': '',
                'canonical_paper_size': tkvue.computed(
                    lambda context: context.paper_sizes_table.get(context.paper_size, context.paper_size)
                ),
                'style_sheet': '',
                'plot_rotation': 0,
                'plot_window': False,
                'x1': '',
                'y1': '',
                'x2': '',
                'y2': '',
                # Processing
                'is_running': False,
                'state': tkvue.computed(lambda context: "disabled" if context.is_running else 'enabled'),
                'plot_window_state': tkvue.computed(
                    lambda context: "disabled" if context.is_running or not context.plot_window else 'enabled'
                ),
            }
        )
        super().__init__(*args, **kwargs)
        # Assign application icon
        fn = pkg_resources.resource_filename(__package__, 'batchcad.png')
        if os.path.isfile(fn):
            self._icon = tkinter.PhotoImage(file=fn, master=self.root)
            self.root.iconphoto(True, [self._icon])
        # Register an event to prompt user for files when application is starting.
        self.acad = None
        self.prompt_event_id = self.root.after(1, self._prompt_filenames)
        # Register listener
        self.data.watch('filenames', self._on_filenames)
        # When plot device get update we need to refresh the list of paper_size
        self.data.watch('plot_device', self._on_plot_device)
        # Configure event
        self.bind('<Destroy>', self._on_destory)
        self.bind('<<on_quit>>', self._on_quit)
        # Configure latest
        self.latest_check = LatestCheck()

    def set_filenames(self, filenames):
        self.data['filenames'] = filenames
        if filenames:
            # Cancel event if files are provided.
            self.root.after_cancel(self.prompt_event_id)

    def _on_destory(self, event):
        if event.widget != self.root:
            return
        # Close AutoCAD document.
        if self.acad and self.acad.doc:
            self.acad.doc.Close(False)

    def _on_filenames(self, filenames):
        if not filenames:
            return

        try:
            self.doc = self.acad.Application.Documents.Open(filenames[0])

            # Get plot devices
            self.data['plot_devices'] = [
                i for i in self.acad.doc.ActiveLayout.GetPlotDeviceNames() if i != 'None' if not i.endswith('.pc3')
            ]
            self.data['plot_device'] = self.doc.ActiveLayout.ConfigName

            # Update plot windows settings
            self.data['plot_window'] = self.doc.ActiveLayout.PlotType == PLOT_TYPE_WINDOW
            p1 = APoint(0, 0)
            p2 = APoint(0, 0)
            self.doc.ActiveLayout.GetWindowToPlot(p1, p2)
            self.data['x1'] = p1[0]
            self.data['y1'] = p1[1]
            self.data['x2'] = p2[0]
            self.data['y2'] = p2[1]
        except Exception:
            tkinter.messagebox.showerror(
                parent=self.root.winfo_toplevel(),
                title='BatchCad',
                message="Impossible to retrieve all plot settings from AutoCad. Make sure AutoCAD is not busy.",
            )

    def _on_plot_device(self, plot_device):
        # Update plot device
        layout = self.acad.doc.ActiveLayout
        try:
            layout.ConfigName = plot_device
            layout.RefreshPlotDeviceInfo()

            # Get PaperSzie information
            paper_sizes = layout.GetCanonicalMediaNames()
            self.data['paper_sizes'] = [layout.GetLocaleMediaName(name) for name in paper_sizes]
            self.data['paper_sizes_table'] = {layout.GetLocaleMediaName(name): name for name in paper_sizes}
            self.data['paper_size'] = layout.GetLocaleMediaName(self.doc.ActiveLayout.CanonicalMediaName)
            self.data['plot_rotation'] = layout.PlotRotation

            # Get plotSyle information
            self.data['style_sheets'] = [''] + list(self.acad.doc.ActiveLayout.GetPlotStyleTableNames())
            if self.doc.ActiveLayout.PlotWithPlotStyles == 1:
                self.data['style_sheet'] = self.doc.ActiveLayout.StyleSheet
            else:
                self.data['style_sheet'] = ''
        except Exception:
            logger.warn('fail to refresh plot device information', exc_info=1)

    def _on_quit(self, unused=None):
        """
        Destroy the windows when user click on Quit button.
        """
        self.root.destroy()

    def _on_start(self, unused=None):
        """
        Start the processing when user click on start button.
        """
        self._plot_task = self.get_event_loop().create_task(self._start_plot())

    def _prompt_filenames(self):
        """
        Prompt user to select file to be printed when dialog is created.
        """
        # Try to get reference to AutoCad. AutoCad must be open.
        try:
            self.acad = Autocad()
            self.acad.app
        except OSError:
            self.acad = None
            logger.exception('Make sure AutoCad is started.')
            tkinter.messagebox.showerror(
                parent=self.root.winfo_toplevel(),
                title='BatchCad',
                message="Make sure AutoCad is started. BatchCad cannot find a running AutoCad instance.",
            )
            self.root.destroy()
            return

        filenames = tkinter.filedialog.askopenfilenames(
            parent=self.root.winfo_toplevel(),
            title='Select AutCad files to be printed',
            initialdir=None,  # TODO
            filetypes=[
                ('Drawing (*.dwg)', '*.dwg'),
                ('All files', '*'),
            ],
            multiple=True,
        )
        if not filenames:
            # Operation cancel by user
            self.root.destroy()
            return
        # Update filenames
        self.data['filenames'] = filenames
        # Register task to check for latest
        self.get_event_loop().create_task(self._check_latest_version_task())

    async def _start_plot(self):
        self.data['is_running'] = True
        try:
            settings = {
                'plot_device': self.data['plot_device'],
                'paper_size': self.data['canonical_paper_size'],
                'style_sheet': self.data['style_sheet'],
                'plot_type': PLOT_TYPE_WINDOW if self.data['plot_window'] else PLOT_TYPE_EXTENDS,
                'plot_rotation': self.data['plot_rotation'],
                'window_to_plot': [
                    (float(self.data['x1'] or '0'), float(self.data['y1'] or '0')),
                    (float(self.data['x2'] or '0'), float(self.data['y2'] or '0')),
                ],
            }
            for f in self.data['filenames']:
                # Leave the loop if thread should be stopped.
                try:
                    await self.get_event_loop().run_in_executor(None, plot, f, *settings.values())
                except StyleSheetError:
                    tkinter.messagebox.showerror(
                        parent=self.root.winfo_toplevel(),
                        title='BatchCad',
                        message="The selected Plot Style is missing or invalid.",
                    )
                except Exception:
                    logger.exception('error during printing process')
                    tkinter.messagebox.showerror(
                        parent=self.root.winfo_toplevel(),
                        title='BatchCad',
                        message="Error during the printing process. Check logs for more details.",
                    )
        finally:
            self.data['is_running'] = False

    async def _check_latest_version_task(self):
        # Query latest version.
        try:
            is_latest = await self.get_event_loop().run_in_executor(None, self.latest_check.is_latest)
            if not is_latest:
                # Show dialog
                self._prompt_latest_version()
        except tkinter.TclError:
            # Swallow exception raised when application get destroyed.
            pass
        except LatestCheckFailed:
            logger.warn('fail to check latest version', exc_info=1)

    def _prompt_latest_version(self):
        latest_version = self.latest_check.get_latest_version()
        ret = tkinter.messagebox.askyesno(
            parent=self.root,
            title=_("New version available"),
            message=_(
                "A new version of BatchCAD (%s) is available from IKUS Software. Do you want to upgrade your copy ?"
            )
            % latest_version,
            detail=_("BatchCAD automatically checks for new updates."),
        )
        if ret == 'no':
            # Operation cancel by user
            return
        # Open URL to Minarca Download page.
        url = self.latest_check.get_download_url()
        webbrowser.open(url)
