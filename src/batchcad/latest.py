# Copyright (C) 2023 IKUS Software. All rights reserved.
# IKUS Software inc. PROPRIETARY/CONFIDENTIAL.
# Use is subject to license terms.
'''
Created on Oct. 22, 2021

@author: Patrik Dufresne <patrik@ikus-soft.com>
'''
import logging
import platform

import requests
from packaging import version

LATEST_VERSION_URL = 'https://latest.ikus-soft.com/batchcad/latest_version'


logger = logging.getLogger(__name__)


class LatestCheckFailed(Exception):
    pass


class LatestCheck:
    """
    Responsible to check if current version is up-to-date.
    """

    def get_download_url(self):
        """
        Return download page.
        """
        return "https://batchcad.org/download/"

    def get_latest_version(self, timeout=0.5):
        """
        Query the latest version of batchcad.
        """
        # Get current version.
        current_version = self.get_current_version()

        try:
            # Replace User agent by something meaningful.
            headers = {
                'User-Agent': f'batchcad/{current_version} python/{platform.python_version()} {platform.system()}/{platform.release()} ({platform.machine()})'
            }
            # Query data
            response = requests.get(LATEST_VERSION_URL, headers=headers, timeout=timeout)
            # Check status
            response.raise_for_status()
            # Parse data as json
            return response.text
        except requests.exceptions.RequestException as e:
            raise LatestCheckFailed(e)

    def get_current_version(self):
        import batchcad

        return batchcad.__version__

    def is_latest(self):
        """
        Check if the current batchcad is up to date.
        """
        # Get current version.
        current_version = self.get_current_version()
        if current_version == 'DEV':
            return True
        try:
            current_version = version.Version(current_version)
        except version.InvalidVersion:
            raise LatestCheckFailed('invalid current_version: ' + current_version)

        # Get latest version
        latest_version = self.get_latest_version()
        try:
            latest_version = version.Version(latest_version)
        except version.InvalidVersion:
            raise LatestCheckFailed('invalid latest_version: ' + latest_version)

        # Compare them
        return current_version >= latest_version
