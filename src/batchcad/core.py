# -*- coding: utf-8 -*-
# BatchCAD - AutCAD Automation
# Copyright (C) 2022 IKUS Soft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import comtypes
from pyautocad import APoint, Autocad

logger = logging.getLogger(__name__)

PAPER_UNITS_MM = 1
PAPER_UNITS_INCHES = 0

PLOT_ROTATION_0 = 0
PLOT_ROTATION_90 = 1

PLOT_TYPE_EXTENDS = 1
PLOT_TYPE_WINDOW = 4

acVpScaleToFit = 0


class PlotError(Exception):
    pass


class StyleSheetError(PlotError):
    pass


def plot(file, plot_device, paper_size, style_sheet, plot_type, plot_rotation, window_to_plot):
    """
    plot the given file.

    ``file`` to be printed
    ``plot_device`` define the printer to be used for printing
    ``paper_size`` define the paper format: later, tabloid, etc.
    ``style_sheet`` Define the style sheet to be used or None
    ``plot_type`` Either Extends or Window
    ``plot_rotation`` Landscape or Portrait
    ``window_to_plot`` the coordinate to be used for Window plot.
    """
    assert file
    assert plot_device
    assert plot_type in [PLOT_TYPE_EXTENDS, PLOT_TYPE_WINDOW]
    assert len(window_to_plot) == 2 and len(window_to_plot[0]) == 2 and len(window_to_plot[1]) == 2
    # Try to get reference to AutoCad. AutoCad must be open.
    try:
        comtypes.CoInitialize()
        acad = Autocad()
        acad.doc.Name
    except OSError:
        raise PlotError("Make sure AutoCAD is started. BatchCad cannot find a running AutoCad instance.")

    # Loop on all document to get them printed.
    logger.info('printing autocad document: %s' % file)
    doc = acad.Application.Documents.Open(file)
    try:
        doc.SetVariable("BACKGROUNDPLOT", 0)
        # Define the printer name.
        doc.ActiveLayout.ConfigName = plot_device
        # Define the paper format.
        doc.ActiveLayout.CanonicalMediaName = paper_size
        # Define the paper unit. 0: inches, 1: mm
        doc.ActiveLayout.PaperUnits = PAPER_UNITS_INCHES
        # Define plot rotation. 0: 0 degree, 1: 90 degree, 2: 180 degree, 3: 270 degree
        doc.ActiveLayout.PlotRotation = plot_rotation
        # Define the plot type. 0: display, 1: extends,
        if plot_type == PLOT_TYPE_WINDOW:
            p1 = APoint(*window_to_plot[0])
            p2 = APoint(*window_to_plot[1])
            doc.ActiveLayout.SetWindowToPlot(p1[:2], p2[:2])
        doc.ActiveLayout.PlotType = plot_type
        # Scale to fit.
        doc.ActiveLayout.StandardScale = acVpScaleToFit
        # TODO Keep origin to 0,0
        # print(acad.doc.ActiveLayout.PlotOrigin)
        doc.ActiveLayout.CenterPlot = True
        if style_sheet:
            doc.ActiveLayout.PlotWithPlotStyles = 1
            try:
                doc.ActiveLayout.StyleSheet = style_sheet
            except Exception:
                raise StyleSheetError(style_sheet)
        else:
            doc.ActiveLayout.PlotWithPlotStyles = 0
        # Force number of copies to 1
        doc.Plot.NumberOfCopies = 1
        # Start printing
        doc.Plot.PlotToDevice()
    finally:
        # Close document
        doc.Close(False)
