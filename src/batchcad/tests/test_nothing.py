import unittest


class TestNothing(unittest.TestCase):
    def test_nothing(self):
        # Nothing to be test since AutoCad might not be installed.
        pass
