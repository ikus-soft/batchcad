# -*- coding: utf-8 -*-
# BatchCAD - AutCAD Automation
# Copyright (C) 2022 IKUS Soft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging
import os
import sys
import tempfile

import pkg_resources

from batchcad.ui import HomeDialog

logger = logging.getLogger(__name__)

try:
    VERSION = pkg_resources.get_distribution("batchcad").version
except pkg_resources.DistributionNotFound:
    VERSION = "DEV"


def parse_args():
    parser = argparse.ArgumentParser("batchcad", description="AutoCad automation")
    parser.add_argument(
        'file', nargs='*', help='list of dwg files to be processed - each file will be open and processed one by one'
    )
    if sys.stdout:
        parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)

    return parser.parse_args()


def main():
    # Configure logging
    logging.basicConfig(
        format='%(asctime)s %(message)s',
        filename=os.path.join(tempfile.gettempdir(), 'batchcad.log'),
        level=logging.INFO,
    )

    # Read arguments
    cfg = parse_args()

    # Open configuration Windows
    dlg = HomeDialog()
    dlg.set_filenames(cfg.file)
    dlg.mainloop()


if __name__ == '__main__':
    main()
