# -*- coding: utf-8 -*-
# BatchCAD - AutCAD Automation
# Copyright (C) 2022 IKUS Soft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import platform
import subprocess
import tempfile
import pkg_resources
import re
from PyInstaller.utils.hooks import copy_metadata
from email import message_from_string
from exebuild import signexe, makensis

#
# Common values
#
icon = 'batchcad.ico'
version = pkg_resources.get_distribution("batchcad").version

#
# Common values
#
icon = 'batchcad.ico'
# Read pacakage info
pkg = pkg_resources.get_distribution("batchcad")
version = pkg.version
_metadata = message_from_string(pkg.get_metadata('METADATA'))
pkg_info = dict(_metadata.items())
long_description = _metadata._payload
block_cipher = None

a = Analysis(
    ['src/batchcad/__main__.py'],
    pathex=[],
    binaries=[],
    datas=copy_metadata('batchcad')
    + [
        ('README.md', '.'),
        ('LICENSE', '.'),
        ('batchcad.ico', '.'),
    ],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

# First executable for windowed mode.
exe_w = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='batchcadw',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=False,
    icon=icon,
    console=False,
)
all_exe = [exe_w]

# Another executable on for console mode.
exe_c = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='batchcad',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=False,
    icon=icon,
    console=True,
)
all_exe += [exe_c]

coll = COLLECT(*all_exe, a.binaries, a.zipfiles, a.datas, strip=False, upx=False, upx_exclude=[], name='batchcad')

# Sign executable
signexe('dist/batchcad/batchcad.exe')
signexe('dist/batchcad/batchcadw.exe')

# Binary smoke test
subprocess.check_call(['dist/batchcad/batchcad.exe', '--version'])

# For NSIS, we need to convert the license encoding
with open('dist/batchcad/LICENSE', 'r', encoding='UTF-8') as input:
    with open('dist/batchcad/LICENSE.txt', 'w', encoding='ISO-8859-1') as out:
        out.write(input.read())

# Create installer using NSIS
exe_version = re.search('.*([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', '0.0.0.' + version).group(1)
nsi_file = os.path.abspath('batchcad.nsi')
setup_file = os.path.abspath('dist/batchcad_%s.exe' % version)
makensis(
    [
        '-NOCD',
        '-INPUTCHARSET',
        'UTF8',
        '-DAppVersion=' + exe_version,
        '-DOutFile=' + setup_file,
        nsi_file,
    ],
    cwd='dist/batchcad',
)

# Sign installer
signexe(setup_file)
